**Group2 Simplon ,Set up Discourse in Azure in under 30 minutes** with zero knowledge of Rails or Linux shell. but these steps will work on any **Docker-compatible** cloud provider or local server. This

<img src="../_resources/Schema_infra.png" alt="Schema_infra.png" width="745" height="349">

walkthrough will go through these in detail:

**[Before you start](#before-you-start)**

1.  [Preparing your domain name](#1-preparing-your-domain-name)
2.  [Setting up email](#2-setting-up-email)

**[Installation](#installation)**

3.  [Create new cloud server](#3-create-new-cloud-server)
4.  [Access new cloud server](#4-access-your-cloud-server)
5.  [Install Discourse](#5-install-discourse)
6.  [Edit Discourse configuration](#6-edit-discourse-configuration)
7.  [Start Discourse](#7-start-discourse)
8.  [Register new account and become admin](#8-register-new-account-and-become-admin)

## Before you start

### 1\. Preparing your domain name

> 🔔 Discourse will not work from an IP address, you must own a domain name such as `example.com` to proceed.

- Already own a domain name? Great. Select a subdomain such as `discourse.example.com` or `talk.example.com` or `forum.example.com` for your Discourse instance.
    
- No domain name? Get one! We can [recommend NameCheap](https://www.namecheap.com/domains/domain-name-search/), or there are many other [great domain name registrars](https://www.google.com/search?q=best+domain+name+registrars) to choose from.
    
- Your DNS controls should be accessible from the place where you purchased your domain name. This is where you will create a DNS [`A` record](https://support.dnsimple.com/articles/a-record/) for the `discourse.example.com` hostname once you know the IP address of the cloud server where you are installing Discourse, as well as enter your [SPF and DKIM records](https://www.google.com/search?q=what+is+spf+dkim) for your email.
    

### 2\. Setting Up Email

> ⚠️ **Email is CRITICAL for account creation and notifications in Discourse.** If you do not properly configure email before bootstrapping YOU WILL HAVE A BROKEN SITE!

> 💡 Email here refers to [Transactional Email](https://www.google.com/search?q=what+is+transactional+email) not the usual email service like Gmail, Outlook and/or Yahoo.

- No existing mail server? Check out our [**Recommended Email Providers for Discourse**](https://github.com/discourse/discourse/blob/main/docs/INSTALL-email.md).
    
- Already have a mail server? Great. Use your existing mail server credentials. (Free email services like Gmail/Outlook/Yahoo do not support transactional emails.)
    
- To ensure mail deliverability, you must add valid [SPF and DKIM records](https://www.google.com/search?q=what+is+spf+dkim) in your DNS. See your mail provider instructions for specifics.
    
- If you're having trouble getting emails to work, follow our [Email Troubleshooting Guide](https://meta.discourse.org/t/troubleshooting-email-on-a-new-discourse-install/16326)
    

## Installation

### 3\. Create New Cloud Server

Create your new cloud server,

The default of **the current supported LTS release of Ubuntu Server** works fine. At minimum, a 64-bit Linux OS with a
modern kernel version is required.

- The default of **1 GB** RAM works fine for small Discourse communities. We recommend 2 GB RAM for larger communities.
    
- The default of **New York** is a good choice for most US and European audiences. Or select a region that is geographically closer to your audience.
    
- Enter your domain `discourse.example.com` as the Droplet name.
    

Create your new Droplet. You may receive an email with the root password, however, [you should set up SSH keys](https://www.google.com/search?q=digitalocean+ssh+keys), as they are more secure.

> ⚠️ Now you have created your cloud server! Go back to your DNS controls and use the IP address to set up an `A record` for your `discourse.example.com` hostname.

### 4\. Access Your Cloud Server

Connect to your server via its IP address using SSH, from azure cloud 

```
ssh root@192.168.1.1 
```

Either use the root password from the email sent you when the server was set up, or have a valid SSH key configured on your local machine.

### 5\. Install Discourse

Clone the [Official Discourse Docker Image](https://github.com/discourse/discourse_docker) into `/var/discourse`.

```
sudo -s
git clone https://github.com/discourse/discourse_docker.git /var/discourse
cd /var/discourse
chmod 700 containers 
```

You will need to be root through the rest of the setup and bootstrap process.

### 6\. Edit Discourse Configuration

Launch the setup tool at

```
./discourse-setup 
```

Answer the following questions when prompted:

```
Hostname for your Discourse? [discourse.example.com]: 
Email address for admin account(s)? [me@example.com,you@example.com]: 
SMTP server address? [smtp.example.com]: 
SMTP port? [587]: 
SMTP user name? [user@example.com]: 
SMTP password? [pa$$word]: 
Let's Encrypt account email? (ENTER to skip) [me@example.com]: 
Optional Maxmind License key () [xxxxxxxxxxxxxxxx]: 
```

You'll get the SMTP details from your [email](#email) setup, be sure to complete that section.

Let's Encrypt account setup is to give you a free HTTPS certificate for your site, be sure to set that up if you want your site secure.

This will generate an `app.yml` configuration file on your behalf, and then kicks off bootstrap. Bootstrapping takes between **2-8 minutes** to set up your Discourse. If you need to change these settings after bootstrapping, you can run `./discourse-setup` again (it will re-use your previous values from the file) or edit `/containers/app.yml` manually with `nano` and then `./launcher rebuild app`, otherwise your changes will not take effect.

### 7\. Start Discourse

Once bootstrapping is complete, your Discourse should be accessible in your web browser via the domain name `discourse.example.com` you entered earlier.

<img src="https://www.discourse.org/images/install/17/discourse-congrats.png" width="650" class="jop-noMdConv">

### 8\. Register New Account and Become Admin

Register a new admin account using one of the email addresses you entered before bootstrapping.

<img src="https://www.discourse.org/images/install/17/discourse-register.png" width="650" class="jop-noMdConv"> <img src="https://www.discourse.org/images/install/17/discourse-activate.png" width="650" class="jop-noMdConv">

(If you are unable to register your admin account, check the logs at `/var/discourse/shared/standalone/log/rails/production.log` and see our [Email Troubleshooting checklist](https://meta.discourse.org/t/troubleshooting-email-on-a-new-discourse-install/16326).)

After registering your admin account, the setup wizard will launch and guide you through basic configuration of your Discourse.

<img src="https://www.discourse.org/images/install/17/discourse-wizard-step-1.png" width="650" class="jop-noMdConv">

After completing the setup wizard, you should see Staff topics and **READ ME FIRST: Admin Quick Start Guide**. This guide contains advice for further configuring and customizing your Discourse install.

![](https://www.discourse.org/images/install/17/discourse-homepage.png)
